import { createStore } from "vuex"
import { State, audiosObject } from "../types/store"

const state: State = {
  deviceOrientation: false
}

const mutations = {
  enableDeviceOrientation (state: State) {
    state.deviceOrientation === true
  },
  registerAudios (state: State, audioData: audiosObject) {
    state.audios = audioData
  }
}

const store = createStore({
   state,
   mutations

})

export default store