export type Targets = 'helen' | 'luis'
export type Speakers = 'L' | 'R' | 'Ls' | 'Rs'

export type Pan = {
  L?: PannerNode,
  R?: PannerNode,
  Ls?: PannerNode,
  Rs?: PannerNode
}

export type Gain = {
  L?: GainNode,
  R?: GainNode,
  Ls?: GainNode,
  Rs?: GainNode
}