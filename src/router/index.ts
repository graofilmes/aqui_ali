import { createWebHistory, createRouter } from "vue-router"
import Intro from "../views/intro.vue"
import Home from "../views/home.vue"
import About from '../views/about.vue'
import Research from '../views/research.vue'

const routes = [
  {
    path: "/",
    name: "Intro",
    component: Intro,
  },
  {
    path: "/experiencias",
    name: "Home",
    component: Home,
  },
  {
    path: "/experiencias/:target?",
    name: "Home",
    component: Home,
  },
  {
    path: '/sobre',
    name: 'about',
    component: About
  },
  {
    path: '/processo',
    name: 'research',
    component: Research
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
