module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'theme-logo': '#253842',
        'theme-blue-dark': '#253742',
        'theme-green-dark': '#384226',
        'theme-yellow-dark': '#c79140',
        'theme-pink-dark': '#ac4a4f',
        'theme-gray-dark': '#97857d',
        'theme-blue-light': '#cfd6da',
        'theme-green-light': '#d0d9bf',
        'theme-yellow-light': '#f6d7a8',
        'theme-pink-light': '#ffdcd7',
        'theme-gray-light': '#d6cdc9'
      },
      screens: {
        'portrait': {'raw': '(orientation: portrait)'}
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
  ]
}
